require_relative './routes/url_shortener.rb'
require 'sinatra/base'
require 'redis'
require 'uri'
require 'securerandom'

class Server < Sinatra::Base
    # Include the url shortener routes
    include UrlShortener

    # Launch webpack dev server
    pid = Process.spawn('./node_modules/.bin/webpack-dev-server --history-api-fallback')
    Process.detach(pid)

    redis = Process.spawn('redis-server')
    Process.detach(redis)

    puts "webpack dev server pid: #{pid}"

    $redis = Redis.new

    helpers do
    	def generate_shortcode
    		SecureRandom.hex(4)
    	end
    end
end