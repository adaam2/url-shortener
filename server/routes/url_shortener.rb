require 'securerandom'
require 'sinatra/json'
require 'uri'

module UrlShortener
    def self.included(app)
        app.post '/shortcode/create' do
            if params[:url] =~ URI::regexp
                s_code = generate_shortcode

                $redis.set("#{s_code}", "#{params[:url]}")

                puts "Valid url"

                json :status => 200, :data => "http://localhost:3001/api/shortcode/#{s_code}"
            else
                json :status => 500, :data => 'Invalid url'
            end
        end

        app.get '/' do
            erb :home
        end

        app.get '/shortcode/:shortcode' do
            url = $redis.get("#{params[:shortcode]}")

            redirect url
        end
    end
end