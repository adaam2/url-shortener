# Setting up

## Installing redis server and running locally

Redis is already included in the repository version, so you can just skip to the `make` command in the instructions below

```
curl http://download.redis.io/releases/redis-3.2.8.tar.gz -LOk
tar zfx redis-3.2.8.tar.gz
cd redis-3.2.8
make
sudo make install
cd ..
```


## Setting up client side

```
bundle install && npm i
```

## Running the app

```
npm run dev
```

And wait a sec until the application starts up (separate redis-server, rackup and webpack-dev-server processes are spawned)

## Viewing app

Go to http://localhost:3001