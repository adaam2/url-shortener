import App from './app';
import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Redirect, Link, browserHistory } from 'react-router'
import UrlShortener from './components/urlshortener.jsx';

render(
<Router history={browserHistory}>

  <Route component={App}>
    <Route name="root" path="/" component={UrlShortener}/>
  </Route>
 
</Router>
, document.getElementById('root'));