import React from 'react';
import reqwest from 'reqwest';
import es6BindAll from "es6bindall";

class App extends React.Component {
  constructor(props) {
    super(props)
    let default_url = 'http://www.adamjamesbull.co.uk'
    this.state = {
      url: default_url,
      short_url: '',
      valid: true
    }
    es6BindAll(this, ["generateShortUrl", "handleChange"])
  }

  handleChange(event) {
    this.setState({
      url: event.target.value
    })
  }

  generateShortUrl(url) {
    let context = this
    reqwest({
        url: `/api/shortcode/create?url=${url}`,
        method: 'post',
        success: function(res) {
          context.setState({
            short_url: res.data,
            valid: res.status === 200
          })
        }
      })
  }

  render() {
    let result = this.state.short_url ? (<a href={this.state.short_url}>{this.state.short_url}</a>) : (<p>Generate a short url!</p>)


    return (
      <div className="url-shortener">
        <input type="text" className={this.state.valid ? 'valid' : 'error'} id="input" value={this.state.url} onChange={this.handleChange}/>
        <button onClick={() => this.generateShortUrl(this.state.url)} className="button">Generate</button>

        <div className="result">
          {result}
        </div>
      </div> 
    );
  }
}

export default App;