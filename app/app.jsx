import React from 'react';
import css from './app.scss';
import { Link } from 'react-router';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="app">
        <header className="main">
          <h1>Short Url Generator</h1>          
        </header>
        {this.props.children}
      </div> 
    );
  }
}

export default App;